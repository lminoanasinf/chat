-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 26-03-2021 a las 10:55:40
-- Versión del servidor: 5.7.24
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_chat_dos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chatrooms`
--

CREATE TABLE `chatrooms` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `msg` varchar(250) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `chatrooms`
--

INSERT INTO `chatrooms` (`id`, `userid`, `msg`, `created_on`) VALUES
(1, 5, 'asdfasdf', '2021-03-24 01:12:19'),
(2, 5, 'asdfasdf', '2021-03-24 01:13:36'),
(3, 4, 'asdfasdf', '2021-03-24 01:13:44'),
(4, 4, 'asdfasdf', '2021-03-24 01:13:46'),
(5, 5, 'asdfasdfasdf', '2021-03-24 01:13:48'),
(6, 5, 'asdfasdfasdf', '2021-03-24 01:13:50'),
(7, 5, 'a', '2021-03-25 01:45:46'),
(8, 5, 'a', '2021-03-25 01:49:08'),
(9, 5, 'a', '2021-03-25 01:49:16'),
(10, 5, 'a', '2021-03-25 01:50:03'),
(11, 5, 'a', '2021-03-25 01:50:06'),
(12, 5, 'a', '2021-03-25 01:50:51'),
(13, 5, 'a', '2021-03-25 01:51:08'),
(14, 5, 'asdfasdfasdf', '2021-03-25 01:51:13'),
(15, 4, 'a', '2021-03-25 01:52:06'),
(16, 5, 'a', '2021-03-25 01:55:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat_message`
--

CREATE TABLE `chat_message` (
  `chat_message_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `chat_message` mediumtext NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Yes','No') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `chat_message`
--

INSERT INTO `chat_message` (`chat_message_id`, `to_user_id`, `from_user_id`, `chat_message`, `timestamp`, `status`) VALUES
(1, 5, 4, 'a', '2021-03-25 04:17:54', 'Yes'),
(2, 4, 5, 'a', '2021-03-25 04:18:11', 'Yes'),
(3, 5, 4, 'a', '2021-03-25 04:18:13', 'Yes'),
(4, 5, 4, 'a', '2021-03-25 04:21:08', 'Yes'),
(5, 5, 4, 'a', '2021-03-25 04:21:31', 'Yes'),
(6, 5, 4, 'a', '2021-03-25 04:24:06', 'Yes'),
(7, 4, 5, 'a', '2021-03-25 04:29:39', 'Yes'),
(8, 4, 5, 'a', '2021-03-25 04:30:13', 'Yes'),
(9, 4, 5, 'a', '2021-03-25 04:31:10', 'Yes'),
(10, 5, 4, 'a', '2021-03-25 04:33:09', 'Yes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat_user_table`
--

CREATE TABLE `chat_user_table` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_profile` varchar(100) NOT NULL,
  `user_created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_verification_code` varchar(50) NOT NULL,
  `user_login_status` enum('Logout','Login') NOT NULL,
  `user_status` enum('Disabled','Enable') NOT NULL,
  `user_token` varchar(100) NOT NULL,
  `user_connection_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `chat_user_table`
--

INSERT INTO `chat_user_table` (`user_id`, `user_name`, `user_email`, `user_password`, `user_profile`, `user_created_on`, `user_verification_code`, `user_login_status`, `user_status`, `user_token`, `user_connection_id`) VALUES
(4, 'Luis', 'email@email.com', 'asdfasdf', 'images/1616589067.png', '2021-03-23 13:46:11', '16a1d722dfa635531b5532861ad87713', 'Login', 'Enable', 'f6781aa7d2b1f37e3344ff085e580669', 83),
(5, 'Juan', 'email1@email.com', 'asdfasdf', 'images/1616589067.png', '2021-03-23 13:50:35', 'd0c2161e69dd4fbc6f1dbbead7f034a4', 'Login', 'Enable', 'b96df6572f29622a9aa880131871a8c0', 50),
(6, 'Pedro', 'email3@email.com', 'asdfasdf', 'images/1616589067.png', '2021-03-24 12:31:07', '4ff03c6b277321d7794306e4e2a39598', 'Logout', 'Enable', 'bf9a121b44f7c3f6c499b72b17d76cad', 104);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `chatrooms`
--
ALTER TABLE `chatrooms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `chat_message`
--
ALTER TABLE `chat_message`
  ADD PRIMARY KEY (`chat_message_id`);

--
-- Indices de la tabla `chat_user_table`
--
ALTER TABLE `chat_user_table`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `chatrooms`
--
ALTER TABLE `chatrooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `chat_message`
--
ALTER TABLE `chat_message`
  MODIFY `chat_message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `chat_user_table`
--
ALTER TABLE `chat_user_table`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
